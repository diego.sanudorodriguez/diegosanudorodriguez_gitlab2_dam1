/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diegosanudorodriguez_gitlab2_dam1;
import java.util.Scanner;
/**
 *
 * @author DAM127
 */
public class DiegoSanudoRodriguez_GitLab2_Dam1 {
    public static void main(String[] args) {
        Scanner teclado=new Scanner(System.in);
        boolean salir=false;
            System.out.println("-----Menu de opciones-----");
            System.out.println("1. Cambio de unidades de horas a segundos.");
            System.out.println("2. Cambio de Km a Metros");
            System.out.println("3. Salir del programa");
            System.out.println("4. Cambio de Km/h a m/s");
            System.out.println("5. Cambio de pulgadas a mm");
            System.out.println("6. Cambio de pies a cm");
            System.out.println("7. Cambio de millas a km");
            int opcion=teclado.nextInt();
            while (salir==false){
            switch (opcion){
                case 1:
                    System.out.println("Introduce las horas: ");
                    int horas=teclado.nextInt();
                    int segundos=horas*3600;
                    System.out.println("Las horas introducidas son: "+segundos+" segundos");
                    salir=true;
                    break;
                case 2:
                    System.out.println("Introduce los Km");
                    int KM=teclado.nextInt();
                    int Metros=KM*1000;
                    System.out.println("Los Km introducidos son: "+Metros+" metros");
                    salir=true;
                    break;
                case 3:
                    System.out.println("Cerrando el programa");
                    salir=true;
                    break;
                case 4:
                    System.out.println("Introduce los Km/h");
                    int kmh=teclado.nextInt();
                    double ms=kmh/3.6;
                    System.out.println("Los Km/H introducidos son: "+ms+" m/s");
                    salir=true;
                    break;
                case 5:
                    System.out.println("Introduce las pulgadas: ");
                    int in=teclado.nextInt();
                    double mm=in*25.4;
                    System.out.println("Las pulgadas introducidas son: "+mm+" mm");
                    salir=true;
                    break;
                case 6:
                    System.out.println("Introduce los pies: ");
                    int pies=teclado.nextInt();
                    double cm=pies*30.48;
                    System.out.println("Los pies introducidos son: "+cm+" cm");
                    salir=true;
                    break;
                case 7:
                    System.out.println("Introduce las millas: ");
                    int millas=teclado.nextInt();
                    double km=millas*1.6093;
                    System.out.println("Las millas introducidas son: "+km+" km");
                    salir=true;
                    break;
                default:
                    System.out.println("El numero introducido no es una opcion, introduce una opcion valida: ");
                    opcion=teclado.nextInt();
                    break;
            }
        }
    }
    
}
